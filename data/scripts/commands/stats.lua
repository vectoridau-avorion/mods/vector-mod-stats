------
-- Stats Command Implementation.

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Module Namespace.
--
-- The following namespace comment is *required* for
-- avorion to recognise the namespace. Do not alter or remove it.
--
-- namespace StatsCommand
StatsCommand = {}

------
-- Retrieve a player instance by the given name or index.
-- @param nameOrIndex the name or index of the player.
-- @return The player, or nil if the player could not be found.
function getPlayerByNameOrIndex(nameOrIndex)
    local faction

    faction = Galaxy():findFaction(nameOrIndex)
    if faction then
        return Player(faction.index)
    end

    local i = 0
    repeat
        i = i + 1
        faction = Galaxy():findFaction(i)
        if faction and faction.name == nameOrIndex then
            return Player(faction.index)
        end
    until not faction

    return nil
end

------
-- Send a message to the target player.
-- @param target The player to send the message to.
-- @param message The message to send (with format specifiers).
-- @param args A table of arguments to populate the format specifiers with.
-- @return nil
function msg(target, message, args)
    target:sendChatMessage(
        "Stats",
        ChatMessageType.Normal,
        message%_t % args
    )
end

------
-- Send stats about the given player to the requester.
-- @param requester The player to send stats to.
-- @param player The player whose stats should be retrieved.
-- @return nil
function sendStats(requester, player)
    local money = player.money
    local resources = {player:getResources()}
    
    msg(requester, "--- begin stats ---", {})
    msg(requester, "Resource stats for player ${player}:", {player = player.name})
    msg(requester, "::: Money: ${value}", {value = money})
    msg(requester, "::: Iron: ${value}", {value = resources[1]})
    msg(requester, "::: Titanium: ${value}", {value = resources[2]})
    msg(requester, "::: Naonite: ${value}", {value = resources[3]})
    msg(requester, "::: Trinium: ${value}", {value = resources[4]})
    msg(requester, "::: Xanion: ${value}", {value = resources[5]})
    msg(requester, "::: Ogonite: ${value}", {value = resources[6]})
    msg(requester, "::: Avorion: ${value}", {value = resources[7]})
    
    -- requester.sendChatMessage("CRAFT", 0, tostring(player.craft))
    -- msg(requester, "Player craft: ${craft}", {craft = player.craft})
    
    local shipNames = {player:getShipNames()}
    if shipNames then
        msg(
            requester,
            "Name(s) of all ${count} ship(s) beloinging to ${player}:",
            {player = player.name, count = #shipNames}
        )
        for i, name in ipairs(shipNames) do
            msg(requester, "::: - ${name}", {name = name})
        end
    end
    
    local craft = player.craft
    if craft ~= nil then
        local craftName = craft.name
        local craftX, craftY = player:getSectorCoordinates()
        local craftDist = math.floor(math.sqrt(craftX * craftX + craftY * craftY) + 0.5)
        msg(requester, "Current Craft for player ${player}:", {player = player.name})
        msg(requester, "::: NAME: ${name}", {name=craftName})
        msg(
            requester,
            "::: LOCATION: \\s(${x}:${y}) [dist to \\s(0:0) = ${dist}]",
            {x=craftX, y=craftY, dist=craftDist}
        )
        if craft.isDrone then
            msg(requester, "::: CRAFT IS A DRONE", {})
        end
    end
    
    msg(requester, "---  end stats  ---", {})
end

------
-- Invoke the stats command.
-- @param sender The index of the player invoking the command.
-- @param commandName
-- @param playerName The name or index of the player to investigate.
-- @param ...
function StatsCommand.execute(sender, commandName, playerName, ...)
    local requester = Player(sender)
    local player = getPlayerByNameOrIndex(playerName)
    if player ~= nil then
        sendStats(requester, player)
    else
        requester:sendChatMessage(
            "STATS",
            ChatMessageType.Information,
            "Could not find player ${player}"%_t % {player = playerName}
        )
    end
   return 0, "", ""
end

------
-- Provide a description of this command.
-- @return A string description of this command.
function StatsCommand.getDescription()
    return "Retrieve statistics about a player"
end

------
-- Provide help for usage of this command.
-- @return A string describing the usage of this command.
function StatsCommand.getHelp()
    return "Retrieve statistics about a player. Usage: /stat [player]"
end
